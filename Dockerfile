FROM node:current-alpine

USER root

WORKDIR /devopscli

RUN apk add --update \
    jq \
    git \
    curl \
    && apk del --purge

# install kubectl
RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" \
    && chmod +x kubectl \
    && mv ./kubectl /usr/local/bin/kubectl \
    && kubectl version --client

# install devops cli
COPY . /devopscli/app
RUN cd /devopscli/app \
    && npm install \
    && npm install -g