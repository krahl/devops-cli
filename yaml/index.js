const YAML = require('js-yaml')
const fs = require('fs')
const yaml = require('yaml');
const _ = require('lodash')

function setyaml(file, key, value) {
    let doc = YAML.load(fs.readFileSync(file, 'utf8'));
     _.set(doc, key, value)    
    const docFinal = new yaml.Document();
    docFinal.contents = doc;
    fs.writeFileSync(file, docFinal.toString())
}

module.exports.setyaml = setyaml
