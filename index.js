#!/usr/bin/env node
const program = require("commander");
const yamlUtils = require('./yaml/index');


program
  .command("set <file> <key> <value>")
  .description("Altera valor de chave de arquivo YAML")
  .action((file, key, value) => {
    yamlUtils.setyaml(file, key, value)
  });

program.parse(process.argv);
